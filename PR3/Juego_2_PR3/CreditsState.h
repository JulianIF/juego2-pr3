#ifndef CREDITSSTATE_H
#define CREDITSSTATE_H
#include <SFML\Graphics.hpp>
#include "MenuState.h"
#include "Web_Request.h"
class CreditsState
{
private:
	sf::Font font;
	sf::Text programador;
	sf::Text assets;
public:
	CreditsState();
	~CreditsState();
	void Run(sf::RenderWindow * window);
};
#endif

#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#include <iostream>
#include <SFML\Graphics.hpp>

using namespace std;
class GameObject 
{
	
protected:
	int lives;
	int moveSpeed;
	sf::Texture texture;
	sf::Sprite sprite;
public:
	float posX;
	float posY;
	int sizeX;
	int sizeY;
	GameObject();
	virtual ~GameObject();
	int GetLives() const;
	void SetLives(const int l);
	int GetMoveSpeed() const;
	void SetMoveSpeed(const int ms);
	sf::Sprite GetSprite() const;
	void SetTexture(const string textureName);
	bool Collide(GameObject* object);
};
#endif

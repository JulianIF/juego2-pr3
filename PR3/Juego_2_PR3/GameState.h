#ifndef GAMESTATE_H
#define GAMESTATE_H
#include "stdafx.h"
#include <iostream>
#include <SFML\Network\Http.hpp>
#include <SFML\json.hpp>
#include <SFML\Audio.hpp>
#include <SFML\Graphics.hpp>
#include "Web_Request.h"
#include "Player1.h"
#include "Player2.h"
#include "Wall.h"
#include "MenuState.h"
#include <vector>
#include <time.h>

using namespace std;
class GameState
{
private:
	sf::Clock clock;
	sf::Font font;
	sf::Text winner;
	sf::Music music;
	bool endGame;
public:
	GameState();
	~GameState();
	void Run(sf::RenderWindow * window);
};
#endif
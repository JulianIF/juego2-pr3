#ifndef WEBREQUEST_H
#define WEBREQUEST_H
#include <iostream>
#include <SFML\Network\Http.hpp>
#include <SFML\json.hpp>
#include <SFML\Graphics.hpp>

using namespace std;

class Web_Request
{
private:
	sf::Font font;
	sf::Text pedido;
public:
	Web_Request();
	~Web_Request();
	int GetWeather(const string ciudad);
};
#endif

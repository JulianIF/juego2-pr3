#ifndef MENUSTATE_H
#define MENUSTATE_H

#include "stdafx.h"
#include <iostream>
#include <SFML\Network\Http.hpp>
#include <SFML\json.hpp>
#include <SFML\Audio.hpp>
#include <SFML\Graphics.hpp>
#include "Web_Request.h"
#include "GameState.h"
#include "CreditsState.h"
#include "Web_Request.h"
class MenuState
{
private:
	sf::Font font;
	sf::Text gameName;
	sf::RenderWindow window;
	sf::Text start;
	sf::Text credits;
	sf::Text ciudad;
	sf::Text leave;
	sf::Color color;
	int clima;
	bool climaPedido;
	Web_Request* request;
public:
	MenuState();
	~MenuState();
	void Run(sf::RenderWindow * window);
};

#endif 

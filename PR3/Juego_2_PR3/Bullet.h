#ifndef BULLET_H
#define BULLET_H
#include "GameObject.h"
class Bullet :
	public GameObject
{
private:
	char dir;
public:
	bool exists = false;
	Bullet(int x, int y, char direction);
	~Bullet();
	void Move(const sf::Time elapsed) ;
};
#endif

#include "GameObject.h"



GameObject::GameObject()
	: 
	lives(0), 
	moveSpeed(0), 
	posX(0), 
	posY(0),
	sizeX(32),
	sizeY(32)
{

}


GameObject::~GameObject()
{

}

void GameObject::SetTexture(const string textureName)
{
	texture.loadFromFile(textureName, sf::IntRect(0, 0, 32, 32));
	sprite.setTexture(texture);
}


bool GameObject::Collide(GameObject * object) 
{
	int distMax = 32;
	float difX = 0;
	float difY = 0;

	difX = this->posX - object->posX;
	difY = this->posY - object->posY;

	if (GetSprite().getGlobalBounds().intersects(object->GetSprite().getGlobalBounds()))
	{
		difX = this->posX - object->posX;
		difY = this->posY - object->posY;

		//DETECTA COLISION DESDE ARRIBA
		if (difY >= -sizeY && difY < -(sizeY-1))
		{
			this->sprite.move(sf::Vector2f(0, -(distMax + difY)));
			posY -= (distMax + difY);
		}
		//DETECTA COLISION DESDE ABAJO
		if (difY >= sizeY-1 && difY < sizeY)
		{
			this->sprite.move(sf::Vector2f(0, (distMax - difY)));
			posY += (distMax - difY);
		}
		//DETECTA COLISION DESDE LA DERECHA
		if (difX >= sizeX-1 && difX < sizeX)
		{
			this->sprite.move(sf::Vector2f((distMax - difX), 0));
			posX += (distMax - difX);
		}
		//DETECTA COLISION DESDE LA IZQUIERDA
		if (difX >= -sizeX && difX < -(sizeX-1))
		{
			this->sprite.move(sf::Vector2f(-(distMax + difX), 0));
			posX -= (distMax + difX);
		}
		return true;
	}	
	else
		return false;
}

int GameObject::GetLives() const 
{
	return lives;
}

int GameObject::GetMoveSpeed() const 
{
	return moveSpeed;
}

void GameObject::SetLives(const int l)
{
	lives = l;
}

void GameObject::SetMoveSpeed(const int ms)
{
	moveSpeed = ms;
}

sf::Sprite GameObject::GetSprite() const
{
	return sprite;
}

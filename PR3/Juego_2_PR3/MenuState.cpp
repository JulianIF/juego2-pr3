#include "MenuState.h"



MenuState::MenuState(): clima(3), climaPedido(false), color(sf::Color::Green)
{
	request = new Web_Request();
}


MenuState::~MenuState()
{
}

void MenuState::Run(sf::RenderWindow * window)
{
	font.loadFromFile("Assets/Fonts/Alef-Regular.ttf");
	//GAME NAME FONT
	gameName.setFont(font);
	gameName.setCharacterSize(40);
	gameName.setFillColor(sf::Color::Black);
	gameName.setPosition(200, 100);
	gameName.setString("WORLD OF TANQUES");
	//START FONT
	start.setFont(font);
	start.setCharacterSize(20);
	start.setFillColor(sf::Color::Black);
	start.setPosition(300, 200);
	start.setString("PRESS S TO START");
	//CREDITS FONT
	credits.setFont(font);
	credits.setCharacterSize(20);
	credits.setFillColor(sf::Color::Black);
	credits.setPosition(300, 300);
	credits.setString("PRESS C FOR CREDITS");
	//CIUDAD FONT
	ciudad.setFont(font);
	ciudad.setCharacterSize(20);
	ciudad.setFillColor(sf::Color::Black);
	ciudad.setPosition(150, 400);
	ciudad.setString("PRESS B FOR BARCELONA OR A FOR BUENOS AIRES");
	//LEAVE FONT
	leave.setFont(font);
	leave.setCharacterSize(20);
	leave.setFillColor(sf::Color::Black);
	leave.setPosition(300, 500);
	leave.setString("PRESS L TO LEAVE");


	while (window->isOpen())//MAIN LOOP
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window->close();
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::L))
		{
			window->close();
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			GameState juego;
			juego.Run(window);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::C))
		{
			CreditsState creditos;
			creditos.Run(window);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::B))
		{
			delete request;
			request = new Web_Request;
			clima = request->GetWeather("Barcelona");
			climaPedido = true;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			delete request;
			request = new Web_Request;
			clima = request->GetWeather("Cordoba");
			climaPedido = true;
		}
		if (climaPedido)
		{
			switch (clima)
			{
			case 0:
				color = sf::Color::Magenta;
				break;
			case 1:
				color = sf::Color::White;
				break;
			case 2:
				color = sf::Color::Yellow;
				break;
			default:
				color = sf::Color::Green;
				break;
			}
		}
		window->clear(color);
		window->draw(gameName);
		window->draw(start);
		window->draw(credits);
		window->draw(ciudad);
		window->draw(leave);
		window->display();
	}
}
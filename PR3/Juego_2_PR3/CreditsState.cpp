#include "CreditsState.h"



CreditsState::CreditsState()
{
}


CreditsState::~CreditsState()
{
}

void CreditsState::Run(sf::RenderWindow * window)
{
	font.loadFromFile("Assets/Fonts/Alef-Regular.ttf");
	//PROGRAMADOR FONT
	programador.setFont(font);
	programador.setCharacterSize(40);
	programador.setFillColor(sf::Color::Green);
	programador.setPosition(100, 100);
	programador.setString("Programador: Julian Fernandez Berro");
	//ASSETS FONT
	assets.setFont(font);
	assets.setCharacterSize(20);
	assets.setFillColor(sf::Color::Green);
	assets.setPosition(200, 200);
	assets.setString("Assets realizados con: Piskel, Bfxr y BoscaCeoil");

	while (window->isOpen())//MAIN LOOP
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window->close();
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			MenuState menu;
			menu.Run(window);
		}

		window->clear(sf::Color::Black);
		window->draw(programador);
		window->draw(assets);
		window->display();
	}
}
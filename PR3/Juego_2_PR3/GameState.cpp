#include "GameState.h"
using namespace std;
GameState::GameState():endGame(false)
{
}


GameState::~GameState()
{
}

void GameState::Run(sf::RenderWindow * window)
{
	font.loadFromFile("Assets/Fonts/Alef-Regular.ttf");
	music.openFromFile("Assets/Audios/musiquita.wav");
	music.setLoop(!endGame);
	music.play();
	music.setVolume(30);
	endGame = false;
	srand(time(NULL));
	int windowHeight = 600;
	int windowWidth = 800;
	Player1* player1 = new Player1(16, 16);
	Player2* player2 = new Player2(windowWidth - 16, 40);
	vector<Wall*> obstaculos;

	float x = 0;
	float y = 0;

	int cubosTotales = 40;

	for (int i = 0; i < cubosTotales; i++)
	{
		x = rand() % (windowWidth - 50) + 50;
		y = rand() % windowHeight;
		Wall* walls = new Wall(x, y);
		/*if (i > 1)
		{
			for (int j = 0; j < obstaculos.size(); j++)
			{
				if (i != j)
					obstaculos[j]->Collide(obstaculos[i - 1]);
			}
		}*/
		obstaculos.push_back(walls);
	}
	/*
	x = 80;
	y = 50;
	Wall* walls = new Wall(x, y);
	obstaculos.push_back(walls);
	x = 80.5;
	y = 50;
	Wall* walls2 = new Wall(x, y);
	obstaculos.push_back(walls2);
	obstaculos[0]->Collide(obstaculos[1]);
	obstaculos[1]->Collide(obstaculos[0]);
	*/
	while (window->isOpen())//MAIN LOOP
	{

		sf::Time elapsed = clock.restart();
		sf::Event event;
		while (window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window->close();
		}
		window->clear(sf::Color::White);
		//------------CHEQUEO DE TECLAS & COLLIDE W/ WALLS------------
		for (int i = 0; i < obstaculos.size(); i++)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && !player1->Collide(obstaculos[i]) && player1->posX > player1->sizeX/2)
			{
				player1->MoveLeft(elapsed);
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && !player1->Collide(obstaculos[i]) && player1->posX < windowWidth - player1->sizeX / 2)
			{
				player1->MoveRight(elapsed);
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && !player1->Collide(obstaculos[i]) && player1->posY > player1->sizeY / 2)
			{
				player1->MoveUp(elapsed);
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && !player1->Collide(obstaculos[i]) && player1->posY < windowHeight - player1->sizeY / 2)
			{
				player1->MoveDown(elapsed);
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) && !player2->Collide(obstaculos[i]) && player2->posX > player2->sizeX / 2)
			{
				player2->MoveLeft(elapsed);
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) && !player2->Collide(obstaculos[i]) && player2->posX < windowWidth - player2->sizeX / 2)
			{
				player2->MoveRight(elapsed);
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && !player2->Collide(obstaculos[i]) && player2->posY > player2->sizeY / 2)
			{
				player2->MoveUp(elapsed);
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) && !player2->Collide(obstaculos[i]) && player2->posY < windowHeight - player2->sizeY / 2)
			{
				player2->MoveDown(elapsed);
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
			{
				sf::Clock c;
				player2->Shoot(elapsed, c.restart());
			}
			
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::RAlt))
			{
				sf::Clock c;
				player1->Shoot(elapsed, c.restart());
			}

			if (player1->bullet->exists)
			{
				player1->bullet->Move(elapsed);
				window->draw(player1->bullet->GetSprite());
			}

			if (player2->bullet->exists)
			{
				player2->bullet->Move(elapsed);
				window->draw(player2->bullet->GetSprite());
			}
		}
		//--------------COLLIDE BULLET VS PLAYERS------------
		if (player1->bullet->exists)
		{
			if (player1->bullet->Collide(player2))
			{
				player2->SetLives(player1->GetDmg());
				player1->bullet->exists = false;
				player1->bullet->SetTexture("Assets/Textures/Empty.png");
			}
		}
		if (player2->bullet->exists)
		{
			if (player2->bullet->Collide(player1))
			{
				player1->SetLives(player2->GetDmg());
				player2->bullet->exists = false;
				player2->bullet->SetTexture("Assets/Textures/Empty.png");
			}
		}
		//--------------COLLIDE BULLET VS WALLS------------
		if (player1->bullet->exists)
		{
			for (int i = 0; i < obstaculos.size(); i++)
			{
				if (player1->bullet->Collide(obstaculos[i]))
				{
					obstaculos[i]->SetLives(player1->GetDmg());
					player1->bullet->exists = false;
					player1->bullet->SetTexture("Assets/Textures/Empty.png");
					if (obstaculos[i]->GetLives() <= 0)
					{
						obstaculos[i]->exists = false;
						obstaculos[i]->SetTexture("Assets/Textures/Empty.png");
					}
				}
			}
		}
		if (player2->bullet->exists)
		{
			for (int i = 0; i < obstaculos.size(); i++)
			{
				if (player2->bullet->Collide(obstaculos[i]))
				{
					obstaculos[i]->SetLives(player2->GetDmg());
					player2->bullet->exists = false;
					player2->bullet->SetTexture("Assets/Textures/Empty.png");
					if (obstaculos[i]->GetLives() <= 0)
					{
						obstaculos[i]->exists = false;
						obstaculos[i]->SetTexture("Assets/Textures/Empty.png");
					}
				}
			}
		}
		//-----------PLAYER KILL----------
		if (player1->GetLives() == 0)
		{
			player1->SetTexture("Assets/Textures/Empty.png");
			winner.setFont(font);
			winner.setCharacterSize(40);
			winner.setFillColor(sf::Color::Green);
			winner.setPosition(200, 100);
			winner.setString("PLAYER 2 WINS");
			endGame = true;
		}
		if (player2->GetLives() == 0)
		{
			player2->SetTexture("Assets/Textures/Empty.png");
			winner.setFont(font);
			winner.setCharacterSize(40);
			winner.setFillColor(sf::Color::Red);
			winner.setPosition(200, 100);
			winner.setString("PLAYER 1 WINS");
			endGame = true;
		}
		//-----------DRAW-----------
		window->draw(player1->GetSprite());
		window->draw(player2->GetSprite());
		if (endGame)
		{
			window->clear(sf::Color::Black);
			for (int i = 0; i < obstaculos.size(); i++)
			{
				delete (obstaculos[i]);
			}
			obstaculos.clear();
			window->draw(winner);
			
		}
		if (player1->bullet->exists)
		{
			player1->bullet->Move(elapsed);
			window->draw(player1->bullet->GetSprite());
		}
			
		if (player2->bullet->exists)
		{
			player2->bullet->Move(elapsed);
			window->draw(player2->bullet->GetSprite());
		}
		
		for (int i = 0; i < obstaculos.size(); i++)
		{
			if (obstaculos[i]->exists)
				window->draw(obstaculos[i]->GetSprite());
			else
			{
				delete (obstaculos[i]);
				obstaculos.erase(obstaculos.begin() + i);
			}
				
		}

		window->display();

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::M) && endGame)
		{
			music.stop();
			MenuState menu;
			menu.Run(window);
		}
	}
	std::cin.clear();
	std::cin.ignore(INT_MAX, '\n');
	std::cin.get();
	delete player1;
	delete player2;
	for (int i = 0; i < obstaculos.size(); i++)
	{
		delete (obstaculos[i]);
	}
	obstaculos.clear();
}
#ifndef PLAYER1_H
#define PLAYER1_H
#include "GameObject.h"
#include "Bullet.h"
#include <SFML\Audio.hpp>
class Player1 :
	public GameObject
{
private:
	int dmg;
	char direction;
	sf::SoundBuffer buffer;
	sf::Sound sound;
public:
	Bullet* bullet;
	Player1(float x, float y);
	~Player1();
	void MoveRight(const sf::Time elapsed);
	void MoveLeft(const sf::Time elapsed);
	void MoveUp(const sf::Time elapsed);
	void MoveDown(const sf::Time elapsed);
	void Shoot(const sf::Time elapsed, const int d);
	void SetLives(const int dmg);
	int  GetDmg() const;
};
#endif

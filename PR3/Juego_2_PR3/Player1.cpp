#include "Player1.h"



Player1::Player1(float x, float y):dmg(1), direction('J')
{
	buffer.loadFromFile("Assets/Audios/Laser_Shoot.wav");
	sound.setBuffer(buffer);
	posX = x;
	posY = y;
	sizeX = 32;
	sizeY = 32;
	lives = 3;
	moveSpeed = 10;
	SetTexture("Assets/Textures/TanqueR.png");
	sprite.setPosition(sf::Vector2f(posX, posY));
	sprite.setOrigin(16, 16);
	bullet = new Bullet(posX, posY, direction);
}


Player1::~Player1()
{
	bullet->exists = false;
	delete bullet;	
}

void Player1::MoveRight (const sf::Time elapsed)
{
	sprite.move(sf::Vector2f(moveSpeed * elapsed.asSeconds(), 0));
	posX += moveSpeed * elapsed.asSeconds();
	sprite.setRotation(-90);
	direction = 'R';
}
void Player1::MoveLeft(const sf::Time elapsed)
{
	sprite.move(sf::Vector2f(-moveSpeed * elapsed.asSeconds(), 0));
	posX += -moveSpeed * elapsed.asSeconds();
	sprite.setRotation(90);
	direction = 'L';
}
void Player1::MoveUp(const sf::Time elapsed)
{
	sprite.move(sf::Vector2f(0, -moveSpeed * elapsed.asSeconds()));
	posY += -moveSpeed * elapsed.asSeconds();
	sprite.setRotation(180);
	direction = 'U';
}
void Player1::MoveDown(const sf::Time elapsed)
{
	sprite.move(sf::Vector2f(0, moveSpeed * elapsed.asSeconds()));
	posY += moveSpeed * elapsed.asSeconds();
	sprite.setRotation(0);
	direction = 'D';
}

void Player1::Shoot(const sf::Time elapsed, const int d) 
{
	dmg = d;
	delete bullet;
	bullet = new Bullet(posX, posY, direction);
	sound.play();
}

void Player1::SetLives(const int dmg)
{
	lives -= dmg;
}

int Player1::GetDmg() const
{
	return dmg;
}
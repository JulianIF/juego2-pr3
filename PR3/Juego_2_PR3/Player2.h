#ifndef PLAYER2_H
#define PLAYER2_H
#include "GameObject.h"
#include "Bullet.h"
#include <SFML\Audio.hpp>
class Player2 :
	public GameObject
{
private:
	int dmg;
	char direction;
	sf::SoundBuffer buffer;
	sf::Sound sound;
public:
	Player2(float x, float y);
	~Player2();
	void MoveRight(const sf::Time elapsed);
	void MoveLeft(const sf::Time elapsed);
	void MoveUp(const sf::Time elapsed);
	void MoveDown(const sf::Time elapsed);
	void Shoot(const sf::Time elapsed, const int d);
	void SetLives(const int dmg);
	int  GetDmg() const;
	Bullet* bullet;
};
#endif

#include "Web_Request.h"



Web_Request::Web_Request()
{
}


Web_Request::~Web_Request()
{
}

int Web_Request::GetWeather(const string ciudad)
{
	try
	{
		sf::Http http("http://query.yahooapis.com");

		sf::Http::Request request;
		request.setUri("/v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + ciudad + "%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");
		sf::Http::Response response = http.sendRequest(request);

		nlohmann::json jdata = nlohmann::json::parse(response.getBody().c_str());

		//std::cout << jdata["query"]["results"]["channel"]["item"]["condition"]["text"];
		if (jdata["query"]["results"]["channel"]["item"]["condition"]["text"] == "Clear")
		{
			return 0;
		}
		else if (jdata["query"]["results"]["channel"]["item"]["condition"]["text"] == "Cloudy")
		{
			return 1;
		}
		else if (jdata["query"]["results"]["channel"]["item"]["condition"]["text"] == "Sunny")
		{
			return 2;
		}
		else
		{
			return 3;
		}
	}
	catch(exception)
	{
		return 5;
	}
}



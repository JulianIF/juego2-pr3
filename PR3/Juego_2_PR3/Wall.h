#ifndef WALL_H
#define WALL_H
#include "GameObject.h"
class Wall :
	public GameObject
{
public:
	bool exists = false;
	Wall(float x, float y);
	~Wall();
	sf::Sprite GetSprite() const;
	void SetLives(const int dmg);
};
#endif

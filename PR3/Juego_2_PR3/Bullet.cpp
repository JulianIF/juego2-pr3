#include "Bullet.h"

Bullet::Bullet(int x, int y, char direction)
	:
	exists(true),
	dir(direction)
{
	posX = x;
	posY = y;
	moveSpeed = 15;
	SetTexture("Assets/Textures/Bullet.png");
	sprite.setPosition(sf::Vector2f(posX, posY));
	sprite.setOrigin(2, 2);
}


Bullet::~Bullet()
{
	exists = false;
}

void Bullet::Move(const sf::Time elapsed) 
{
	switch (dir)
	{
		case 'D':
			sprite.move(sf::Vector2f(0, moveSpeed * elapsed.asSeconds()));
			posY += moveSpeed * elapsed.asSeconds();
			break;
		case 'U':
			sprite.move(sf::Vector2f(0, -moveSpeed * elapsed.asSeconds()));
			posY += -moveSpeed * elapsed.asSeconds();
			break;
		case 'R':
			sprite.move(sf::Vector2f(moveSpeed * elapsed.asSeconds(), 0));
			posX += moveSpeed * elapsed.asSeconds();
			break;
		case 'L':
			sprite.move(sf::Vector2f(-moveSpeed * elapsed.asSeconds(), 0));
			posX += -moveSpeed * elapsed.asSeconds();
			break;
	}
}
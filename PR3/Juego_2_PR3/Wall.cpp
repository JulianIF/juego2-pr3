#include "Wall.h"



Wall::Wall(float x, float y)
	:
	exists(true)
{
	posX = x;
	posY = y;
	sizeX = 32;
	sizeY = 32;
	lives = 3;
	SetTexture("Assets/Textures/Obstacle.png");
	sprite.setPosition(sf::Vector2f(posX, posY));
	sprite.setOrigin(16, 16);
}


Wall::~Wall()
{
	exists = false;
}

sf::Sprite Wall::GetSprite() const
{
	return sprite;
}

void Wall::SetLives(const int dmg)
{
	lives -= dmg;
}
